//
//  ExpenseDetailViewController.m
//  ObjCExpenseTracker
//
//  Created by Douglas Sass on 2/16/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ExpenseDetailViewController.h"

@interface ExpenseDetailViewController ()

@end

@implementation ExpenseDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    //Text Field to enter the location of the expense
    expLocationTxtFld = [UITextField new];
    expLocationTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    expLocationTxtFld.placeholder = @"Location";
    expLocationTxtFld.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:expLocationTxtFld];
    
    //Date Picker that allows the selection of the date of the expense
    expDatePicker = [UIDatePicker new];
    expDatePicker.datePickerMode = UIDatePickerModeDate;
    expDatePicker.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expDatePicker];
    
    //Text Field to enter the cost of the expense
    expCostTxtFld = [UITextField new];
    expCostTxtFld.placeholder = @"Enter cost of expense in format ##.##";
    expCostTxtFld.backgroundColor = [UIColor whiteColor];
    expCostTxtFld.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expCostTxtFld];
    
    //TextView to enter notes in for the expense
    expNotesTxtVw = [UITextView new];
    expNotesTxtVw.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:expNotesTxtVw];

    //Sets the mlayout of all the views
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(expLocationTxtFld,expDatePicker,expCostTxtFld,expNotesTxtVw);
    
    NSDictionary *metrics = @{@"margin":[NSNumber numberWithInteger:20], @"topmargin":[NSNumber numberWithInteger:70]};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expLocationTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[expDatePicker]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expCostTxtFld]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-margin-[expNotesTxtVw]-margin-|" options:0 metrics:metrics views:viewsDictionary]];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topmargin-[expLocationTxtFld(40)]-5-[expDatePicker]-5-[expCostTxtFld(40)]-5-[expNotesTxtVw(80)]" options:0 metrics:metrics views:viewsDictionary]];
    
    //Add button to save changes to the expense
    UIBarButtonItem* saveExpenseBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBtnTouched:)];
    
    //Share button to email the expense
    UIBarButtonItem* shareExpenseBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionBtnTouched:)];
    
    //Places the Add button and Share button to the navigation bar
    self.navigationItem.rightBarButtonItems = @[saveExpenseBtn, shareExpenseBtn];
    
    //If the expense exists, display the expense
    if (self.expense) {
        expLocationTxtFld.text = [self.expense valueForKey:@"expLocation"];
        expDatePicker.date = [self.expense valueForKey:@"expDate"];
        expCostTxtFld.text = [self.expense valueForKey:@"expCost"];
        expNotesTxtVw.text = [self.expense valueForKey:@"expNotes"];

    }
}


//Sets up the email to send the expense
-(void)actionBtnTouched:(id)sender {
    
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    
    [mailCont setSubject:@"Expenses"];
    NSString* message = [NSString stringWithFormat:@"Lcoation of Expense: %@\n\nDate of Expense: %@\n\nCost of Expense: $%@\n\nNotes: %@", expLocationTxtFld.text, expDatePicker.date, expCostTxtFld.text, expNotesTxtVw.text];
    [mailCont setMessageBody:message isHTML:NO];
    
    [self presentViewController:mailCont animated:YES completion:^{
        
    }];
}

// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//Action when the save button is touched
-(void)saveBtnTouched:(id)sender {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    //Saves changes to the expense
    if (self.expense) {
        [self.expense setValue:expLocationTxtFld.text forKey:@"expLocation"];
        [self.expense setValue:expDatePicker.date forKey:@"expDate"];
        [self.expense setValue:expCostTxtFld.text forKey:@"expCost"];
        [self.expense setValue:expNotesTxtVw.text forKey:@"expNotes"];
    } else {
        //Creates a new expense
        NSManagedObject *newExpense = [NSEntityDescription insertNewObjectForEntityForName:@"Expense" inManagedObjectContext:context];
        [newExpense setValue:expLocationTxtFld.text forKey:@"expLocation"];
        [newExpense setValue:expDatePicker.date forKey:@"expDate"];
        [newExpense setValue:expCostTxtFld.text forKey:@"expCost"];
        [newExpense setValue:expNotesTxtVw.text forKey:@"expNotes"];
        
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
