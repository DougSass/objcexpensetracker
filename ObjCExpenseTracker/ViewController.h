//
//  ViewController.h
//  ObjCExpenseTracker
//
//  Created by Douglas Sass on 2/16/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExpenseDetailViewController.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

{
    //Table view for the list of expenses
    UITableView* myExpensesTableView;
    
    //Array to contain each expense and display them in the table view
    NSMutableArray* arrayOfExpenses;
}


@end

