//
//  ExpenseDetailViewController.h
//  ObjCExpenseTracker
//
//  Created by Douglas Sass on 2/16/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>

@interface ExpenseDetailViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    
    //Expense location
    UITextField* expLocationTxtFld;
    
    //Expense date
    UIDatePicker* expDatePicker;
    
    //Expense Cost
    UITextField* expCostTxtFld;
    
    //Expense Notes
    UITextView* expNotesTxtVw;
    
}

@property (nonatomic, strong) NSManagedObject* expense;

@end
