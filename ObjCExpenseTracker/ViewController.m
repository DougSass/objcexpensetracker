//
//  ViewController.m
//  ObjCExpenseTracker
//
//  Created by Douglas Sass on 2/16/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Creates the myExpensesTableView and how it is displayed
    myExpensesTableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    myExpensesTableView.translatesAutoresizingMaskIntoConstraints = NO;
    myExpensesTableView.delegate = self;
    myExpensesTableView.dataSource = self;
    [self.view addSubview:myExpensesTableView];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(myExpensesTableView);
    
    NSDictionary *metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[myExpensesTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[myExpensesTableView]|" options:0 metrics:metrics views:viewsDictionary]];
    
    //Creates the Add Expense button
    UIBarButtonItem* addExpenseBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addBtnTouched:)];
    self.navigationItem.rightBarButtonItem = addExpenseBtn;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];
}

-(void)loadData {
    AppDelegate* appD = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSManagedObjectContext *context = appD.persistentContainer.viewContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Expense"];
    NSArray* array = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    arrayOfExpenses = [array mutableCopy];
    [myExpensesTableView reloadData];
}

//when add Epxense Button is touched it will push to the ExpenseDetailViewController
-(void)addBtnTouched:(id)sender {
    ExpenseDetailViewController* edvc = [ExpenseDetailViewController new];
    [self.navigationController pushViewController:edvc animated:YES];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSManagedObject* object = arrayOfExpenses[indexPath.row];
    
    
    
    cell.textLabel.text = [object valueForKey:@"expLocation"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return arrayOfObjects.count;
    return arrayOfExpenses.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ExpenseDetailViewController* edvc = [ExpenseDetailViewController new];
    edvc.expense = arrayOfExpenses[indexPath.row];
    [self.navigationController pushViewController:edvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end


